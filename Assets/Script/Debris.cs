﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Debris : MonoBehaviour
{
    public Rigidbody2D rb;
    public float randomX;
    public float randomY;
    public int rotationInit;

    public bool stop;
    // Start is called before the first frame update
    void Start()
    {
        randomX = Random.Range(-0.05f, 0.05f);
        randomY = Random.Range(-0.05f, 0.05f);
        rotationInit = Random.Range(1, 360);
        
        rb.AddForce(new Vector2(randomX,randomY), ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        rb.rotation = rotationInit;

        if (stop)
        {
            rb.velocity = Vector2.zero;
        }
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        stop = true;
    }

}

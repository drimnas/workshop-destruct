﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class Meuble : MonoBehaviour
{
    public int life;

    public bool collied;

    public GameObject sharp;

    public GameObject sharp2;

    public int numberSharp;

    public int weapon;

    public AudioSource sound; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (collied && Input.GetButtonDown("Jump"))
        {

            if (weapon >0)
            {
                life -= 4;
                weapon--;
                
            }
            sound.Play();
            life--;
        }

        if (life <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            collied = true;
         weapon =  other.gameObject.GetComponent<Player>().weaponLife;
         
        
        }
    }
    

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            collied = false;
        }
    }

    private void OnDestroy()
    {
        for (int i = 0; i < numberSharp; i++)
        {
         //   float randomX = Random.Range(-1, 1);
          //  float randomY = Random.Range(-1, 1);
            var obj1 = Instantiate(sharp, new Vector2(transform.position.x ,transform.position.y ), quaternion.identity);
            var obj2 = Instantiate(sharp2, new Vector2(transform.position.x ,transform.position.y ), quaternion.identity);
        }
    }
}

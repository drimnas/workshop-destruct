﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotDestroy : MonoBehaviour
{
    public GameObject[] listAudio;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        listAudio = GameObject.FindGameObjectsWithTag("Music");

        for (int i = 1; i <listAudio.Length ; i++)
        {
            Destroy(listAudio[i]);
        }
        
        DontDestroyOnLoad(this.gameObject);
    }
}

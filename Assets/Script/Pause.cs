﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject menu;
    
    // Start is called before the first frame update
    void Start()
    {
        menu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            menu.SetActive(true);
        }

        if (menu.active)
        {
            Time.timeScale = 0; 
        }

        if (menu.active == false) 
        {
            Time.timeScale = 1; 
          
        }
    }

    public void Resume()
    {
        menu.SetActive(false);
    }

    public void Quit() 
    {
        Application.Quit();
    }
}

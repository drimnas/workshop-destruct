﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed;

    public float moveInputH;
    public float moveInputV;

    public int weaponLife;

    public TMP_Text textLife;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        moveInputH = Input.GetAxis("Horizontal");
        moveInputV = Input.GetAxis("Vertical");
        
        transform.Translate(new Vector2(moveInputH, moveInputV)* speed * Time.deltaTime);

        textLife.text = weaponLife.ToString();

    }


    private void OnCollisionExit2D(Collision2D other)
    {
        weaponLife = other.gameObject.GetComponent<Meuble>().weapon;
    }
}

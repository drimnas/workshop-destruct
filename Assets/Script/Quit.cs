﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Quit : MonoBehaviour
{
    public void Start()
    {
        Destroy(GameObject.FindGameObjectWithTag("Music"));
    }

    public void Replay()
    {
        SceneManager.LoadScene("Niveau_01");
    }
    
    public void QuitButton()
    {
        Application.Quit();
    }
}

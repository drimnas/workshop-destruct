﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    public GameObject[] allObject;

    public int numberFurniture;

    public TMP_Text textScore;
    // Start is called before the first frame update
    void Start()
    {
        allObject = GameObject.FindGameObjectsWithTag("Meuble");
        
    }

    // Update is called once per frame
    void Update()
    {
        allObject = GameObject.FindGameObjectsWithTag("Meuble");
        
        numberFurniture = allObject.Length;

        textScore.text = numberFurniture.ToString();
    }
}

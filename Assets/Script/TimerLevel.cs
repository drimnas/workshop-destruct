﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimerLevel : MonoBehaviour
{
    public int timerValue;

    private float _timer;

    public TMP_Text timerText;

    public GameObject[] debris; 
    // Start is called before the first frame update
    void Start()
    {
        debris = GameObject.FindGameObjectsWithTag("Debris");

        if (debris.Length > 0)
        {
            for (int i = 0; i < debris.Length; i++)
            {
                Destroy(debris[i]);
            }
        }
        
        _timer = timerValue;
    }

    // Update is called once per frame
    void Update()
    {

        if (_timer >= 0)
        {
            _timer -= Time.deltaTime;
        }

        if (_timer <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        timerText.text = Mathf.FloorToInt(_timer +1).ToString();

    }
}
